import de.vandermeer.asciitable.AsciiTable;

import java.sql.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        //Scanner wird instanziert
        Scanner scanner = new Scanner(System.in);

        //Wilkommensnachricht wird ausgegeben
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" + "\n  Wilkommen zur Sitzplatzvermietung" + "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");

        Boolean loggedIn = true;
        while (loggedIn) {
            //Es wird das Menü ausgegeben und die Auswahl gespeichert
            System.out.println("Was möchten Sie tun?\n1: Spielplan anzeigen\n2: Gäste anzeigen\n3: Verfügbarkeit der Sitzplätze\n4: Sitzplatz buchen\n5: Mannschaft erfassen\n6: Spiel erfassen\n7: Gast erfasen\n8: Applikation beenden");

            int auswahl = ControllerMethods.integerValidation();

            switch (auswahl) {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    Team.mannschaftErfassen();
                    break;
                case 6:
                    Game.spielErfassen();
                    break;
                case 7:
                    break;
                case 8:
                    loggedIn = false;
                    System.out.println("Die Applikation wird beendet...");
                    break;
                default:
                    System.out.println("Bitte eine gültige Auswahl treffen");
            }
        }
    }
}
