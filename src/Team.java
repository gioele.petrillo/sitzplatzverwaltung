import de.vandermeer.asciitable.AsciiTable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Team {

    public void showMannschaft() throws SQLException, ClassNotFoundException {
        Statement statement = MysqlConnector.connectToDB();


        //Die Mannschaften werden abgerufen
        ResultSet resultSet = statement.executeQuery("SELECT * FROM team");

        //Die AsciiTabelle wird instanziert und ausgegeben
        AsciiTable at = new AsciiTable();
        System.out.println();
        at.addRule();
        at.addRow("ID", "Mannschaft");
        at.addRule();
        while (resultSet.next()) {
            at.addRow(resultSet.getInt(1), resultSet.getString(2));
            at.setPaddingLeft(2);
            at.addRule();
        }
        System.out.println(at.render());
    }

    public static void mannschaftErfassen() throws SQLException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        //Titel wird ausgegeben
        System.out.println("\n---------------------------------\n" + "-------Mannschaft erfassen-------\n" + "---------------------------------\n\n");
        System.out.println("Manschafftsname: ");

        //Der name der Mannschaft wird erfasst
        String name = scanner.nextLine();


        //Die Eingabe wird überprüft
        Boolean falsch = true;
        while (falsch) {
            if (name.equals("") || name.equals(" ")) {
                System.out.println("Bitte einen gültigen Mannschaftsnamen eingeben:");
                name = scanner.nextLine();
            } else {
                falsch = false;
            }
        }

        //Das statement wird geholt
        Statement statement = MysqlConnector.connectToDB();

        //Prepared statement wird gemacht
        PreparedStatement preparedStatement = MysqlConnector.getConnection().prepareStatement("INSERT INTO team (name) VALUES(?)");

        //Der name wird gesetzt
        preparedStatement.setString(1, name);

        //Das Statement wird ausgeführt
        preparedStatement.executeUpdate();

        //Nachricht, dass die Mannschaft erfasst wurde
        System.out.println(name + " wurde erfasst\n");

        //Die Verbindung wird geschlossen
        MysqlConnector.closeConnection();
    }
}
