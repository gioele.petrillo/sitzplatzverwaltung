import java.time.LocalDate;

public class Guest {
    private String name;
    private String lastname;
    private String address;
    private LocalDate birthday;
    private String phone;
    private String mobilePhone;
    private String email;

    public Guest(String name, String lastname, String address, LocalDate birthday, String phone, String mobilePhone, String email) {
        this.name = name;
        this.lastname = lastname;
        this.address = address;
        this.birthday = birthday;
        this.phone = phone;
        this.mobilePhone = mobilePhone;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
