import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class ControllerMethods {

    public static int integerValidation() {
        int auswahl = 0;
        Scanner scanner = new Scanner(System.in);
        Boolean falsch = true;
        while (falsch) {
            try {
                String i = scanner.next();
                auswahl = Integer.parseInt(i);
                falsch = false;
            } catch (Exception e) {
                System.out.println("Bitte eine Zahl eingeben");
            }
        }
        return auswahl;
    }
}
