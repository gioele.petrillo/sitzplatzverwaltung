import java.sql.*;

public class MysqlConnector {
    private static Connection connection;

    public static Connection getConnection() {
        return connection;
    }

    public static void setConnection(Connection connection) {
        MysqlConnector.connection = connection;
    }

    public static Statement connectToDB() throws ClassNotFoundException, SQLException {
        //Der driver wird geladen
        Class.forName("com.mysql.cj.jdbc.Driver");

        //Die Daten für die Verbindung
        final String dbUrl = "jdbc:mysql://localhost:2234/seatreservation?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=CET";
        final String username = "root";
        final String password = "DerStein123";

        //Die Verbindung wird hergestellt
        Connection connection = DriverManager.getConnection(dbUrl, username, password);

        //Die Connection wird gesetzt
        setConnection(connection);

        //Das Statement wird erstellt
        Statement statement = connection.createStatement();

        return statement;
    }

    public static void closeConnection() throws SQLException {
        //Die Verbindung wird geschlossen
        getConnection().close();
    }
}
